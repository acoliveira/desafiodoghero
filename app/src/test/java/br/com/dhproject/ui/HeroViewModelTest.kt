package br.com.dhproject.ui

import android.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.dhproject.data.model.Hero
import br.com.dhproject.data.model.User
import br.com.dhproject.data.repository.HeroRepository
import br.com.dhproject.util.any
import br.com.dhproject.util.capture
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.*
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class HeroViewModelTest {
    @Rule
    @JvmField
    val taskExecutorRule = InstantTaskExecutorRule()

    @InjectMocks
    private lateinit var viewModel: HeroViewModel

    @Mock
    private lateinit var repository: HeroRepository

    @Captor
    private lateinit var callbacks: ArgumentCaptor<HeroRepository.HeroCallbacks>

    private val hero = Hero().apply {
        is_superhero = true
        address_neighborhood = "Parque Chacabuco"
        price = 55f
        User("Eduardo", "https://doghero-uploads.s3.amazonaws.com/uploads/photo/1433381/sq135_DH_24012018123600937_98895.png")
    }
    private val heroes = ArrayList<Hero>().apply { add(hero) }
    private val messageError = "erro ao carregar herois"

    @Test
    fun callsRecentesRepository() {
        viewModel.onLoadRecents()
        Mockito.verify(repository).loadRecentsHeroes(any())
    }

    @Test
    fun callsFavoritesRepository() {
        viewModel.onLoadFavorites()
        Mockito.verify(repository).loadFavoritesHeroes(any())
    }

    @Test
    fun showsProgressBarRecents() {
        viewModel.onLoadRecents()
        Assert.assertThat(viewModel.isProgressBarVisible.get(), CoreMatchers.`is`(true))
    }

    @Test
    fun showsProgressBarFavorites() {
        viewModel.onLoadFavorites()
        Assert.assertThat(viewModel.isProgressBarVisible.get(), CoreMatchers.`is`(true))
    }

    @Test
    fun progressBarIsInvisibleInitially() {
        MatcherAssert.assertThat(viewModel.isProgressBarVisible.get(), CoreMatchers.`is`(false))
    }

    @Test
    fun onLoadRecentsSuccess() {
        viewModel.onLoadRecents()
        Mockito.verify(repository).loadRecentsHeroes(capture(callbacks))
        callbacks.value.onLoadHeroesRecentsSuccess(heroes.toMutableList())

        Assert.assertThat(viewModel.isProgressBarVisible.get(), CoreMatchers.`is`(false))
        Assert.assertEquals(viewModel.recentsMutableList.value?.get(0)?.user, heroes[0].user)
    }

    @Test
    fun onLoadFavoritesSuccess() {
        viewModel.onLoadFavorites()
        Mockito.verify(repository).loadFavoritesHeroes(capture(callbacks))
        callbacks.value.onLoadHeroesFavoritesSuccess(heroes.toMutableList())

        Assert.assertThat(viewModel.isProgressBarVisible.get(), CoreMatchers.`is`(false))
        Assert.assertEquals(viewModel.favoritesMutableList.value?.get(0)?.user, heroes[0].user)
    }

    @Test
    fun onLoadRecentesError() {
        viewModel.onLoadRecents()
        Mockito.verify(repository).loadRecentsHeroes(capture(callbacks))
        callbacks.value.onLoadHerosRecentsError(messageError)

        Assert.assertThat(viewModel.isProgressBarVisible.get(), CoreMatchers.`is`(false))
        Assert.assertEquals(viewModel.showErrorToast.value, messageError)
    }

    @Test
    fun onLoadFavoritesError() {
        viewModel.onLoadFavorites()
        Mockito.verify(repository).loadFavoritesHeroes(capture(callbacks))
        callbacks.value.onLoadHeroesFavoritesError(messageError)

        Assert.assertThat(viewModel.isProgressBarVisible.get(), CoreMatchers.`is`(false))
        Assert.assertEquals(viewModel.showErrorToast.value, messageError)
    }
}