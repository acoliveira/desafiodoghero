package br.com.dhproject

import br.com.dhproject.injection.Injector
import br.com.dhproject.injection.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class ApplicationDH : DaggerApplication() {

    companion object {
        lateinit var instance: ApplicationDH
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Injector.init(this)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }
}