package br.com.dhproject.util

import br.com.dhproject.R

fun imageFavorite(isFavorite: Boolean) : Int {
    return if (isFavorite) {
        R.drawable.icon_like_filled_vector_red
    } else {
        R.drawable.icon_like_border_vector_gray_battleship
    }
}