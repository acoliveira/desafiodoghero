package br.com.dhproject.util

import android.support.v4.app.Fragment
import br.com.dhproject.R
import br.com.dhproject.ui.HeroesFragment
import br.com.dhproject.util.BottomNavigationPosition.*

enum class BottomNavigationPosition(val position: Int, val id: Int) {
    SEARCH(0, R.id.search),
    MESSAGE(1, R.id.message),
    HEROES(2, R.id.heroes),
    RESERVE(3, R.id.reserve),
    PROFILE(3, R.id.profile);
}

fun findNavigationPositionById(id: Int): BottomNavigationPosition  {
    var position = HEROES
    if(id == HEROES.id) {
         position = HEROES
    }
    return position
}

fun BottomNavigationPosition.createFragment(): Fragment {
    var fragment = Fragment()
    if (this == HEROES) {
        fragment = HeroesFragment.newInstance()
    }
    return fragment
}
fun BottomNavigationPosition.getTag(): String {
    var tag = ""
    if (this == HEROES) {
        tag = HeroesFragment.TAG
    }
    return tag
}

