package br.com.dhproject.injection.module

import br.com.dhproject.ui.HeroesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FragmentModule {
    @ContributesAndroidInjector
    fun heroFragment(): HeroesFragment
}
