package br.com.dhproject.injection.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import br.com.dhproject.injection.ViewModelFactory
import br.com.dhproject.injection.ViewModelKey
import br.com.dhproject.ui.HeroViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {
    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HeroViewModel::class)
    fun bindBannerViewModel(viewModel: HeroViewModel): ViewModel

}