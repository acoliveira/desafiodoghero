package br.com.dhproject.injection.module

import br.com.dhproject.data.repository.HeroRepository
import br.com.dhproject.data.repository.HeroRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {
    @Binds
    fun bindHeroRepository(heroRepositoryImpl: HeroRepositoryImpl): HeroRepository

}
