package br.com.dhproject.injection.component

import android.app.Application
import br.com.dhproject.ApplicationDH
import br.com.dhproject.injection.module.FragmentModule
import br.com.dhproject.injection.module.RepositoryModule
import br.com.dhproject.injection.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    FragmentModule::class,
    ViewModelModule::class,
    RepositoryModule::class,
    AndroidSupportInjectionModule::class
])
interface AppComponent : AndroidInjector<ApplicationDH> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}
