package br.com.dhproject.ui


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.VisibleForTesting
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import br.com.dhproject.R
import br.com.dhproject.data.model.Hero
import br.com.dhproject.databinding.FragmentHeroesBinding
import br.com.dhproject.injection.Injectable
import br.com.dhproject.util.BindingAdapters
import javax.inject.Inject

class HeroesFragment : Fragment() , Injectable {

    @Inject
    @VisibleForTesting
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var dataBinding: FragmentHeroesBinding

    private lateinit var viewModel: HeroViewModel

    companion object {
        val TAG: String = HeroesFragment::class.java.simpleName
        fun newInstance() = HeroesFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(HeroViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        viewModel.onLoadRecents()
        viewModel.onLoadFavorites()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_heroes, container, false)
        dataBinding = FragmentHeroesBinding.bind(rootView)

        bindRecents()
        bindFavorites()
        return dataBinding.root
    }

    private fun bindRecents() {
        dataBinding.recyclerViewRecents.adapter = HeroesAdapter(emptyList())
        dataBinding.recyclerViewRecents.layoutManager = LinearLayoutManager(activity)
        dataBinding.viewModel = viewModel

        val observer = Observer<MutableList<Hero>> { t ->
            t?.apply {
                val twoRecents = this.take(2)
                BindingAdapters.setItems(dataBinding.recyclerViewRecents, twoRecents.toMutableList())
            }
        }
        viewModel.recentsMutableList.observe(this, observer)
        viewModel.showErrorToast.observe(this, Observer {
            Toast.makeText(activity, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun bindFavorites() {
        dataBinding.recyclerViewFavorites.adapter = HeroesAdapter(emptyList())
        dataBinding.recyclerViewFavorites.layoutManager = LinearLayoutManager(activity)
        dataBinding.viewModel = viewModel

        val observer = Observer<MutableList<Hero>> { t ->
            t?.apply {
                val threeFavorites = this.take(3)
                BindingAdapters.setItems(dataBinding.recyclerViewFavorites, threeFavorites.toMutableList())
            }
        }
        viewModel.favoritesMutableList.observe(this, observer)
        viewModel.showErrorToast.observe(this, Observer {
            Toast.makeText(activity, it, Toast.LENGTH_LONG).show()
        })
    }
}
