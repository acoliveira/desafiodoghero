package br.com.dhproject.ui

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import br.com.dhproject.data.model.Hero
import br.com.dhproject.data.repository.HeroRepository
import br.com.dhproject.util.SingleLiveEvent
import javax.inject.Inject

class HeroViewModel @Inject constructor(private val heroRepository: HeroRepository) : ViewModel() {
    var recentsMutableList = MutableLiveData<MutableList<Hero>>()
    var favoritesMutableList = MutableLiveData<MutableList<Hero>>()
    val isProgressBarVisible = ObservableBoolean()
    val showErrorToast = SingleLiveEvent<String>()

    init {
        isProgressBarVisible.set(false)
    }

    fun onLoadRecents() {
        isProgressBarVisible.set(true)
        heroRepository.loadRecentsHeroes(HeroCallbacks())
    }

    fun onLoadFavorites() {
        isProgressBarVisible.set(true)
        heroRepository.loadFavoritesHeroes(HeroCallbacks())
    }

    inner class HeroCallbacks : HeroRepository.HeroCallbacks {
        override fun onLoadHeroesRecentsSuccess(heroes: MutableList<Hero>) {
            isProgressBarVisible.set(false)
            recentsMutableList.value = heroes.toMutableList()
        }

        override fun onLoadHerosRecentsError(messageError: String) {
            isProgressBarVisible.set(false)
            showErrorToast.value = messageError
        }

        override fun onLoadHeroesFavoritesSuccess(heroes: MutableList<Hero>) {
            isProgressBarVisible.set(false)
            heroes.forEach {
                it.is_favorite = true
            }
            favoritesMutableList.value = heroes.toMutableList()
        }

        override fun onLoadHeroesFavoritesError(messageError: String) {
            isProgressBarVisible.set(false)
            showErrorToast.value = messageError
        }
    }
}