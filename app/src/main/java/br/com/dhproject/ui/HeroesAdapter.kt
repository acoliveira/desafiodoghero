package br.com.dhproject.ui

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.dhproject.R
import br.com.dhproject.data.model.Hero
import br.com.dhproject.databinding.HeroItemBinding
import br.com.dhproject.util.AdapterItemsContract
import br.com.dhproject.util.imageFavorite


class HeroesAdapter (var heroes: List<Hero>) : RecyclerView.Adapter<HeroesAdapter.ViewHolder>(),
    AdapterItemsContract {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.hero_item, parent, false)
        val binding = HeroItemBinding.bind(view)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
       return heroes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(heroes[position], this)
    }

    override fun replaceItems(list: List<*>) {
        this.heroes = list as List<Hero>
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: HeroItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(hero: Hero, adapter: HeroesAdapter){
            binding.imageViewHostList.visibility = if (hero.is_superhero) View.VISIBLE else View.GONE
            binding.imageButtonFavorite.setImageResource(imageFavorite(hero.is_favorite))

            binding.imageButtonFavorite.setOnClickListener {
                hero.is_favorite = !hero.is_favorite
                adapter.notifyDataSetChanged()
            }
            binding.hero = hero
        }
    }
}

