package br.com.dhproject.data.model

import java.io.Serializable

data class User(
    var first_name: String = "",
    var image_url: String = "") : Serializable

