package br.com.dhproject.data.remote.response

import br.com.dhproject.data.model.Hero

data class ResponseHero (
    var recents: ArrayList<Hero>,
    var favorites: ArrayList<Hero>
)