package br.com.dhproject.data.repository

import br.com.dhproject.ApplicationDH
import br.com.dhproject.R
import br.com.dhproject.data.api.getMyHeroes
import br.com.dhproject.data.executor.network
import br.com.dhproject.data.executor.ui
import br.com.dhproject.data.remote.response.ResponseHero
import com.beust.klaxon.Klaxon
import java.text.ParseException
import javax.inject.Inject

class HeroRepositoryImpl  @Inject constructor() : HeroRepository {
    override fun loadRecentsHeroes(callbacks: HeroRepository.HeroCallbacks) {
        network {
            try {
                val heroes = Klaxon().parse<ResponseHero>(getMyHeroes())
                heroes?.apply {
                    ui {callbacks.onLoadHeroesRecentsSuccess(recents)}
                } ?: run {
                    ui { callbacks.onLoadHerosRecentsError(ApplicationDH.instance.getString(R.string.error_in_load_list)) }
                }
            } catch (e: Exception) {
                e.message?.apply {
                    ui { callbacks.onLoadHerosRecentsError(this) }
                }
            }
        }
    }

    override fun loadFavoritesHeroes(callbacks: HeroRepository.HeroCallbacks) {
        network {
            try {
                val heroes = Klaxon().parse<ResponseHero>(getMyHeroes())
                heroes?.apply {
                    ui {callbacks.onLoadHeroesFavoritesSuccess(favorites)}
                } ?: run {
                    ui { callbacks.onLoadHeroesFavoritesError(ApplicationDH.instance.getString(R.string.error_in_load_list)) }
                }
            } catch (e: ParseException) {
                e.message?.apply {
                    ui { callbacks.onLoadHeroesFavoritesError(this) }
                }
            }
        }
    }
}

