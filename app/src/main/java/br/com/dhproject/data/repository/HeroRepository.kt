package br.com.dhproject.data.repository

import android.support.annotation.MainThread
import br.com.dhproject.data.model.Hero

interface HeroRepository {
    fun loadRecentsHeroes(callbacks: HeroCallbacks)

    fun loadFavoritesHeroes(callbacks: HeroCallbacks)

    interface HeroCallbacks {
        @MainThread
        fun onLoadHeroesRecentsSuccess(heroes: MutableList<Hero>)

        @MainThread
        fun onLoadHerosRecentsError(messageError: String)

        @MainThread
        fun onLoadHeroesFavoritesSuccess(heroes: MutableList<Hero>)

        @MainThread
        fun onLoadHeroesFavoritesError(messageError: String)
    }
}