package br.com.dhproject.data.model

import java.io.Serializable

data class Hero(
    var is_superhero: Boolean = false,
    var is_favorite: Boolean = false,
    var user: User? = null,
    var address_neighborhood: String = "",
    var price: Float = 0f
) : Serializable {
    val priceFormated: String get() = price.toInt().toString()
}


